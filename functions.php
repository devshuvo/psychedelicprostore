<?php
/**
 * Psychedelicprostore functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Psychedelicprostore
 */

if ( ! function_exists( 'psychedelicprostore_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function psychedelicprostore_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Psychedelicprostore, use a find and replace
		 * to change 'psychedelicprostore' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'psychedelicprostore', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'psychedelicprostore' ),
			'menu-2' => esc_html__( 'Footer Menu', 'psychedelicprostore' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'psychedelicprostore_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		/**
		 * Register Custom Navigation Walker
		 */
		require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';
	}
endif;
add_action( 'after_setup_theme', 'psychedelicprostore_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function psychedelicprostore_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'psychedelicprostore_content_width', 640 );
}
add_action( 'after_setup_theme', 'psychedelicprostore_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function psychedelicprostore_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'psychedelicprostore' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'psychedelicprostore' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );

	for($i=1;$i<=4;$i++){
		register_sidebar( array(
			'name'          => esc_html__( "Footer $i", 'psychedelicprostore' ),
			'id'            => "footer-$i",
			'description'   => esc_html__( 'Add widgets here.', 'psychedelicprostore' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h5 class="widget-title">',
			'after_title'   => '</h5>',
		) );
	}

}
add_action( 'widgets_init', 'psychedelicprostore_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function psychedelicprostore_scripts() {

	$ver = current_time( 'timestamp' );

	wp_enqueue_style( 'bootstrap',get_template_directory_uri() . '/assets/css/bootstrap.css' );
	wp_enqueue_style( 'font-awesome',get_template_directory_uri() . '/assets/css/font-awesome.min.css' );
	wp_enqueue_style( 'main-style',get_template_directory_uri() . '/assets/css/style.css' );
	wp_enqueue_style( 'main-style','http://fonts.googleapis.com/css?family=Open+Sans' );
	wp_enqueue_style( 'psychedelicprostore-style', get_stylesheet_uri(), null, $ver );

	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/plugins/bootstrap.js', array('jquery'), '20151215', true );
	wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/assets/js/custom.js', array('jquery'), '20151215', true );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'psychedelicprostore_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}


// Theme Options
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Options',
		'menu_title'	=> 'Theme Options',
		'menu_slug' 	=> 'theme-general-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

function bs_container(){
	echo "container";
}
function bs_row(){
	echo "row";
}

// Add to cart button text change
add_filter('woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text');
add_filter('woocommerce_product_add_to_cart_text', 'woo_custom_cart_button_text');

function woo_custom_cart_button_text() {
	return __('Order Now','‘woocommerce’');
}

/**
 * WooCommerce specific scripts & stylesheets.
 *
 * @return void
 */
function psychedelicprostore_woocommerce_scripts() {
	wp_enqueue_style( 'psychedelicprostore-woocommerce-style', get_template_directory_uri() . '/woocommerce.css' );
	
}
add_action( 'wp_enqueue_scripts', 'psychedelicprostore_woocommerce_scripts',200 );

function hudai(){
	/**
	 * Hook: woocommerce_before_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * Hook: woocommerce_before_shop_loop_item_title.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item' );
}

remove_action( 'woocommerce_before_shop_loop_item_title','woocommerce_show_product_loop_sale_flash',10 );
remove_action( 'woocommerce_before_shop_loop_item_title','woocommerce_template_loop_product_thumbnail',10 );

add_action( 'woocommerce_after_shop_loop_item','woocommerce_show_product_loop_sale_flash',99 );
add_action( 'woocommerce_after_shop_loop_item','woocommerce_template_loop_product_thumbnail',99 );

add_action( 'woocommerce_after_shop_loop_item_title','wc_show_tag',15 );
function wc_show_tag(){
	global $product;
	$output = array();

	// get an array of the WP_Term objects for a defined product ID
	$terms = wp_get_post_terms( get_the_id(), 'product_tag' );

	// Loop through each product tag for the current product
	if( count($terms) > 0 ){
	    foreach($terms as $term){
	        $term_id = $term->term_id; // Product tag Id
	        $term_name = $term->name; // Product tag Name
	        $term_slug = $term->slug; // Product tag slug
	        $term_link = get_term_link( $term, 'product_tag' ); // Product tag link

	        // Set the product tag names in an array
	        $output[] = '<div class="woocommerce-loop-category__title maybe-last" href="'.$term_link.'">'.$term_name.'</div>';
	    }
	    // Set the array in a coma separated string of product tags for example
	    //$output = implode( ', ', $output );
	    $output = $output[0]; // show only first tag

	    // Display the coma separated string of the product tags
	    echo $output;
	}
}

add_action( 'woocommerce_after_shop_loop_item_title','enable_extra_data',15 );
function enable_extra_data(){
	global $product;
	$output = "";
	
	$enable_extra_data = get_field('enable_extra_data', get_the_id() );

	if( $enable_extra_data == "Yes" ){		
		if( have_rows('extra_datas') ):		 	
		    while ( have_rows('extra_datas') ) : the_row();

		        $name = get_sub_field('name');
		        $value = get_sub_field('value');

		        $output .= '<div class="woocommerce-loop-category__title extra-data">';
			        if( $name != "" ){
			        	$output .= '<span class="ext_name">'.$name.'</span> / ';
			        }
			        if( $value != "" ){
			        	$output .= '<span class="ext_value">'.$value.'</span>';
			        }
		        $output .= '</div>';

		    endwhile;
		else :
		endif;

	}
	  
	echo $output;	
}



// Backend: Add and display a custom field for variable products
add_action('woocommerce_product_options_general_product_data', 'add_custom_product_general_field');
function add_custom_product_general_field()
{
    global $post;

    echo '<div class="options_group hide_if_variable hide_if_external">';

    woocommerce_wp_text_input(array(
        'id'          => 'unit_price',
        'label'       => __('Min Unit price', 'woocommerce') ,
        'placeholder' => '',
        'description' => __('Enter the minimum unit price here.', 'woocommerce'),
        'desc_tip'    => 'true',
    ));

    echo '</div>';
}

// Backend: Save the custom field value for variable products
add_action('woocommerce_process_product_meta', 'save_custom_product_general_field');
function save_custom_product_general_field($post_id)
{
    if (isset($_POST['unit_price'])){
        $min_unit_price = sanitize_text_field($_POST['unit_price']);
        // Cleaning the min unit price for float numbers in PHP
        $min_unit_price = str_replace(array(',', ' '), array('.',''), $min_unit_price);
        // Save
        update_post_meta($post_id, 'unit_price', $min_unit_price);
    }
}



function cw_change_product_price_display( $price ) {
	global $product;
	$unit_price = get_post_meta( $product->id, 'unit_price', true );
	if ( ! empty( $unit_price ) ) {
		$price .= "<span class='unit-per'> / {$unit_price}</span>";
	}

		
	return $price;
}
add_filter( 'woocommerce_get_price_html', 'cw_change_product_price_display' );
add_filter( 'woocommerce_cart_item_price', 'cw_change_product_price_display' );