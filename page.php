<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Psychedelicprostore
 */

get_header();
?>

<section class="container">
    <div class="row">
        <div class="col-sm-9 col-form">
            <div class="left-col">
	        <?php
			while ( have_posts() ) :
				the_post();

				the_content();		

			endwhile; // End of the loop.
			?>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="sidebar">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</section>



<?php
get_footer();
