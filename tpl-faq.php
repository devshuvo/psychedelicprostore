<?php
/**
 * Template Name: F.A.Q
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Psychedelicprostore
 */

get_header();
?>

<section class="container">
    <div class="row">
        <div class="col-sm-9 col-form">
            <div class="left-col">
	        <?php
			while ( have_posts() ) :
				the_post();

				the_content();	?>


                <?php if(get_field('product_faqs')): ?>
                    <?php $i = 0; ?>

                    <h3><u>PRODUCT</u></h3>
                    <div class="panel-group" id="accordion">

                    <?php while(has_sub_field('product_faqs')): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $i; ?>" class="collapsed"><?php the_sub_field('question'); ?></a></h4></div>
                            <div id="collapse_<?php echo $i; ?>" class="panel-collapse collapse" style="height: 0px;">
                                <div class="panel-body">
                                    <?php the_sub_field('answer'); ?>
                                </div>
                            </div>
                        </div>

                    <?php $i++; ?>
                    <?php endwhile; ?>
                    </div>
                <?php endif; ?>

                <?php if(get_field('shipping_faqs')): ?>
                    <?php $i = 0; ?>

                    <h3><u>SHIPPING</u></h3>
                    <div class="panel-group" id="ship_accordion">

                    <?php while(has_sub_field('shipping_faqs')): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse_ship_<?php echo $i; ?>" class="collapsed"><?php the_sub_field('question'); ?></a></h4></div>
                            <div id="collapse_ship_<?php echo $i; ?>" class="panel-collapse collapse" style="height: 0px;">
                                <div class="panel-body">
                                    <?php the_sub_field('answer'); ?>
                                </div>
                            </div>
                        </div>

                    <?php $i++; ?>
                    <?php endwhile; ?>
                    </div>
                <?php endif; ?>
    
                <div class="bottom-cont">
                    <?php the_field('bottom_content'); ?>
                </div>
    



    

			<?php endwhile; // End of the loop.
			?>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="sidebar">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</section>



<?php
get_footer();
