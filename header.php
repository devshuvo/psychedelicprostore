<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Psychedelicprostore
 */

$custom_logo_id = get_theme_mod( 'custom_logo' );
$image_alt = get_post_meta( $custom_logo_id, '_wp_attachment_image_alt', true );
 if ( empty( $image_alt ) ) {
    $image_alt = get_bloginfo( 'name', 'display' );
}
$logo_url = wp_get_attachment_image_url( $custom_logo_id, 'full' );

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<!-- 
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'psychedelicprostore' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif;
			$psychedelicprostore_description = get_bloginfo( 'description', 'display' );
			if ( $psychedelicprostore_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $psychedelicprostore_description; /* WPCS: xss ok. */ ?></p>
			<?php endif; ?>
		</div>

		<nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'psychedelicprostore' ); ?></button>
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',
				'menu_id'        => 'primary-menu',
			) );
			?>
		</nav>
	</header>

	<div id="content" class="site-content">
-->

<body <?php body_class(); ?>>
    
    <div class="navbar navbar-inverse navbar-fixed-top" >

		<?php if( have_rows('header_top_widgets','option') ): ?>
			<?php $i = 0; ?>
			<div class="nav-top">
				<div class="container">
					<div class="row">
	
						<?php while( have_rows('header_top_widgets','option') ): the_row(); 
	
							// vars
							$image = get_sub_field('image');
							$text = get_sub_field('text');
							$link = get_sub_field('link');
	
							?>
	
							<div class="col-sm-3 htc-<?php echo $i; ?>">
								<img src="<?php echo $image; ?>" class="ic"> <a href="<?php echo $link; ?>"><?php echo $text; ?></a>
							</div>
						<?php $i++; ?>
						<?php endwhile; ?>
	
					</div>
				</div>
			</div>
	
		<?php endif; ?>


        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!--a class="navbar-brand" href="#"><font color="orange"><b>PSYCHEDELIC PRO STORE</font></b></a-->
                <h1><a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo $logo_url; ?>" class="img-responsive logo" alt="<?php echo $image_alt; ?>"></a></h1>
            </div>

			<?php 
			wp_nav_menu( array(
			    'theme_location' => 'menu-1',
			    'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
			    'container'       => 'div',
			    'container_class' => 'collapse navbar-collapse',
			    'container_id'    => 'bs-example-navbar-collapse-1',
			    'menu_class'      => 'nav navbar-nav navbar-right',
			    'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
			    'walker'          => new WP_Bootstrap_Navwalker(),
			) );
			?>

           
        </div>
    </div>

	<?php if( (is_page() && !is_front_page()) || is_single() || is_shop() || is_404() ):
	
		$header_bg = "";
		if( has_post_thumbnail() ){
			$header_bg = "background-image: url(".get_the_post_thumbnail_url().");";
		}else if( get_header_image() ){
			$header_bg = "background-image: url(".get_header_image().");";
		}


		?>
	    <section id="home" class="head-main-img" style="<?php echo $header_bg; ?>">
	         
	        <div class="container">
	           	<div class="row text-center pad-row">
	            	<div class="col-md-12">
	              		<h2><big><b>  <?php the_title(); ?>  </b></big></h2>
	               	</div>
	            </div>
	        </div>   
           
       </section>
    <?php endif; ?>