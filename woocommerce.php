<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Psychedelicprostore
 */

get_header();
?>
<section id="products">
    <div class="container">        
        <div class="row">
            <?php woocommerce_content(); ?>
        </div>

    </div>
</section>
<?php
get_footer();
