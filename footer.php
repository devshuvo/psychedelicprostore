<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Psychedelicprostore
 */

?>


<section class="container">
    <img src="<?php echo get_template_directory_uri();?>/assets/img/divider.png" alt="">
</section>

<section class="container">

    <h2 align="center" class="foot-top-h2"><font color="green">
		<i><br />.... 100% Safe and Risk Free World Wide Delivery......</i><br />WE DO WHOLESALES AND OFFER DISCOUNTS TO ALL CLIENTS WITH BULK ORDERS</font> 
	</h2>
</section>

<?php if( have_rows('card_logos','option') ): ?>

<section class="container">
    <div class="row">

	<?php while( have_rows('card_logos','option') ): the_row(); 

		// vars
		$image = get_sub_field('image');
		$link = get_sub_field('link_to');

		?>

        <div class="col-sm-3">
            <div class="thumbnail">
                <a href="<?php echo $link; ?>"><img src="<?php echo $image; ?>" class="img-responsive" alt="partners"></a>
            </div>
        </div>

	<?php endwhile; ?>

    </div>
</section>

<?php endif; ?>






<section id="footer-sec">
	<div class="container">        
        <div class="row">
        	<div class="col-md-3">
            	<?php dynamic_sidebar( 'footer-1' ); ?>
            </div>
        	<div class="col-md-3">
            	<?php dynamic_sidebar( 'footer-2' ); ?>
            </div>
        	<div class="col-md-3">
            	<?php dynamic_sidebar( 'footer-3' ); ?>
            </div>
        	<div class="col-md-3">
            	<?php dynamic_sidebar( 'footer-4' ); ?>
            </div>        
        </div>
    </div>
    <div class="container">
        <div class="text-center">
            <?php 
            wp_nav_menu( array(
                'theme_location' => 'menu-2',
                'depth'           => 1, 
                'after' => "<span> | </span>",
                'menu_class'      => 'footer-menu',
            ) );
            ?>
            <p>&nbsp;</p>
        </div>
    </div>
    <div class="footer-bot">
        <div class="container">
            <div class="text-center">
                <p><?php the_field('footer_copyright_text','option');?></p>
            </div>
        </div>
    </div>
</section>
<?php wp_footer(); ?>

</body>
</html>