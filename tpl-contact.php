<?php
/**
 * Template Name: Contact
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Psychedelicprostore
 */

get_header();
?>

     <section class="contact-page" style="background-image: url(<?php the_field('section_background_image'); ?>);">
     <div class="container">
     <div class="row">
        <div class="col-sm-8">

        <div class="cognito">
            <?php
            while ( have_posts() ) :
                the_post();

                the_content();      

            endwhile; // End of the loop.
            ?>
        </div>
        
        </div>
        <div class="col-sm-4">
            <div class="contact-infos">
                <?php the_field('contact_info'); ?>
            </div>
        </div>        
        </div>
        </div>
     </section>
<?php
get_footer();
