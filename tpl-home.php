<?php
/**
 * Template Name: Homepage
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Psychedelicprostore
 */

get_header();
?>

<?php if(get_field('slider')): ?>
    <?php $i = 0; ?>
    <?php $j = 0; ?>
    <section id="home" class="text-center">
        <div id="carousel-example" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">

                <?php while(has_sub_field('slider')): ?>
                    <div class="item <?php echo ($i==0) ? "active" : ""; ?>">
                        <img src="<?php the_sub_field('image'); ?>" alt="buy real weed online" />
                        <div class="carousel-caption ">
                            <h2><?php the_sub_field('text'); ?></h2>
                        </div>
                    </div>
                    <?php $i++; ?>
                <?php endwhile; ?>
        
            </div>
            <ol class="carousel-indicators">
                <?php while(has_sub_field('slider')): ?>
                    <li data-target="#carousel-example" data-slide-to="<?php echo $j; ?>" class="<?php echo ($j==0) ? "active" : ""; ?>"></li>
                    <?php $j++; ?>
                <?php endwhile; ?>
            </ol>
        </div>
    </section>
<?php endif; ?>


          


<?php if(get_field('portfolios_box')): ?>
<section class="container">
    <center>
        <img src="<?php echo get_template_directory_uri();?>/assets/img/divider.png" alt="" />
    <center>
</section>
<section id="portfolio">
    <div class="container">
        <div class="row">
            <?php while(has_sub_field('portfolios_box')): ?>
                <div class="col-md-3">
                    <a href="<?php the_sub_field('link'); ?>">
                        <h3 align="center"><font color="green"><b><?php the_sub_field('text'); ?></font></b></h3>
                        <div class="thumbnail">
                            <img src="<?php the_sub_field('image'); ?>" width="100%" height="100%" alt="Magic truffles for sale">
                        </div>
                    </a>
                </div>
            <?php endwhile; ?>        
        </div>
    </div>
</section>

<?php endif; ?>


<img src="<?php echo get_template_directory_uri();?>/assets/img/divider.png" alt="" /><br/>
<div class="home-about" style="background-image: url(<?php the_field('background_image'); ?>);">
    <div class="container">
        <div class="row">
            <h2 class="text-center"><?php the_field('title'); ?></h2>
            <div class="col-sm-4">
                <div class="thumbnail">
                    <figure class="abt"><img alt="" src="<?php the_field('thumbnail'); ?>" class="img-responsive"></figure>
                </div>
            </div>
            <div class="col-sm-8">
                <?php the_field('content'); ?>
            </div>
        </div>
    </div>
</div>

<?php if(get_field('product_grid')): ?>
    <?php while(has_sub_field('product_grid')): ?>
        <?php 
        $term =  get_sub_field('category');
        $limit =  get_sub_field('limit');

        ?>

        <section id="portfolio">
            <div class="container">
                <img src="<?php echo get_template_directory_uri();?>/assets/img/divider.png" alt="" />
                <h3 class="text-center"><font color="green"><big><b><?php the_sub_field('grid_title'); ?></big></b></font> </h3>
                <br />
                <div class="row">               

                    <?php echo do_shortcode("[products limit='{$limit}' columns='4' category='{$term->slug}' cat_operator='AND']"); ?>

                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>






<section class="container">
    <?php the_content(); ?>

</section>



<?php
get_footer();
